package com.github.welblade.aula05;
/*
Pratica
Aula 05 — Estruturas condicionais (parte 07; vídeo 26)

Exercício 3

Dado um número de 1 à 7, imprima se é dia útil ou final de semana,
considere domingo como 1, use estrutura switch.
 */

public class EstruturasCondicionaisSwitch {
    public static void main(String[] args) {
        int diaDaSemana = 1;

        switch (diaDaSemana) {
            case 1:
            case 7:
                System.out.println("Final de semana");
                break;
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                System.out.println("Dia útil");
            default:
                System.out.println("Opção inválida");
        }
    }
}
