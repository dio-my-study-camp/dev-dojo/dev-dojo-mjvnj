package com.github.welblade.aula05;
/*
Pratica
Aula 05 — Estruturas condicionais (parte 04; vídeo 23)

Exercício 2

Calcular o valor do imposto sobre a renda anual usado
as taxas dos Países Baixos (netherlands) como base.
 */

public class EstruturasCondicionaisIf {
    public static void main(String[] args) {
        double limiteRenda1 = 35410.0;
        double limiteRenda2 = 69398.0;

        double rendaAnual = 30000.0;
        double taxaImposto;

        if(rendaAnual < limiteRenda1) {
            taxaImposto = 9.42 / 100;
        } else if (rendaAnual < limiteRenda2 ) {
            taxaImposto = 37.07 / 100;
        } else {
            taxaImposto = 49.50 / 100;
        }

        double impostoDevido = rendaAnual * taxaImposto;
        System.out.println("Volor do imposto: € " + impostoDevido);
    }
}
