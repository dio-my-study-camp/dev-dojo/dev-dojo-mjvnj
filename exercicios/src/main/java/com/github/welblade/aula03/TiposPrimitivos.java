package com.github.welblade.aula03;
/*
Pratica
Aula 03 — Tipos Primitivos (parte 05; vídeo 14)

Exercício 1

Crie variáveis para os campos descritos abaixo entre <> e imprima a seguinte mensagem:

Eu <nome>, morando no endereço <endereço>,
confirmo que recebi o salário de <salario> na <data>
 */
public class TiposPrimitivos {
    public static void main(String[] args) {
        String nome = "João Silva";
        String endereco = "Uberlândia/MG - Brasil";
        double salario = 9000.0;
        String dataRecebimento = "17/09/2022";
        String mensagem = "Eu "+ nome + ", morando no " + endereco + "," +
        " confirmo que recebi o salário de " + salario+ " na data "+ dataRecebimento + ".";

        System.out.println(mensagem);
    }
}
