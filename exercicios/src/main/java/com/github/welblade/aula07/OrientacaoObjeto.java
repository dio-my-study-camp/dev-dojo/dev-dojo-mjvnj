package com.github.welblade.aula07;
/*
Pratica
Aula 07 — Orientação a Objeto (parte 02; vídeo 42)

Exercício 6

Crie uma classe carro com atributos nome, modelo e ano, e imprima.
 */
public class OrientacaoObjeto {
       public static void main(String[] args) {
        Carro fordKa = new Carro("Ford", "Ka", 2006);
        Carro fusca = new Carro("Volkswagen", "Fusca", 1980);

        System.out.println(fordKa.nome);
        System.out.println(fordKa.modelo);
        System.out.println(fordKa.ano);

        System.out.println(fusca.nome);
        System.out.println(fusca.modelo);
        System.out.println(fusca.ano);
    }
}

