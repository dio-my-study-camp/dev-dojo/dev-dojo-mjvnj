package com.github.welblade.aula07;

public class Carro {
    String nome;
    String modelo;
    int ano;

    public Carro(String nome, String modelo, int ano) {
        this.nome = nome;
        this.modelo = modelo;
        this.ano = ano;
    }
}
