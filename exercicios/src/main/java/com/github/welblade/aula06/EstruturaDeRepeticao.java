package com.github.welblade.aula06;
/*
Pratica
Aula 06 — Estruturas de repetição (parte 02; vídeo 28)

Exercício 4

Imprima os números pares de 0 até 1.000.000
 */
public class EstruturaDeRepeticao {
    public static void main(String[] args) {
        for (int i = 0; i <= 1000000; i++) {
            if (i % 2 == 0) {
                System.out.println(i);
            }
        }
    }
}
