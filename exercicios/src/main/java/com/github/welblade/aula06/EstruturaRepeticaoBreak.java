package com.github.welblade.aula06;

/*
Pratica
Aula 06 — Estruturas de repetição (parte 04; vídeo 30)

Exercício 5

Dado o valor de um carro, descobrir em quantas vezes pode ser parcelado
em parcelas de R$ 1000.
 */

public class EstruturaRepeticaoBreak {
    public static void main(String[] args) {
        final int precoCarro = 40000;

        for (int i = 1; i < 10000; i++) {
            final double parcela = precoCarro / i;
            if (precoCarro / i < 1000) {
                break;
            }
            System.out.println(i + "x parcelas de R$ " + parcela);
        }
    }
}
